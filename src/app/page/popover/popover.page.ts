import { Component, OnInit } from '@angular/core';
import { NavController, Platform, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(
    public navCtrl : NavController,
    public pl : Platform,
    public popover : PopoverController
  ) { }

  ngOnInit() {
  }

  about(){
    this.navCtrl.navigateForward(['about'])
    this.popover.dismiss()
  }
  exit(){
    navigator['app'].exitApp();
    this.popover.dismiss()
  }

}
