import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public iab: InAppBrowser
  ) { }

  ngOnInit() {
  }

  Home() {
    this.navCtrl.navigateRoot('home')
  }

  openUrl() {
    const browser = this.iab.create('https://covid19.mathdro.id/api');
    browser.show()
  }
  openApi2() {
    const browser = this.iab.create('https://kawalcovid19.harippe.id/api/summary');
    browser.show()
  }

  openCV() {
    const browser = this.iab.create('http://arkanarafka.tech/');
    browser.show()
  }
  wa(){
    const browser = this.iab.create('https://wa.me/6285210093953');
    browser.show()
  }
  linkedin(){
    const browser = this.iab.create('https://www.linkedin.com/in/fauzy-agustian-27b83273/');
    browser.show()
  }
  fb(){
    const browser = this.iab.create('https://www.facebook.com/agustian.fauzi.1');
    browser.show()
  }

}
