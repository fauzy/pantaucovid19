/**
 * @author Fauzi Agustian (arkanarafka.tech)
 * @class ApiService
 * @api v1
 * @description This is class for calling API Services and this is class for global other class. You dont need editing this class.
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  Api_url : any
  Api_url_2 : any
  constructor() { 
    console.log("API service by fauzi");
    this.Api_url = 'https://covid19.mathdro.id/api/'
    this.Api_url_2 = 'https://kawalcovid19.harippe.id/api/'
  }

  getAll(){
    return this.Api_url+'confirmed'
  }
  getByCountry(){
    return this.Api_url+'countries/'
  }
  get_global(){
    return this.Api_url
  }

  get_indo(){
    return this.Api_url_2+'summary'
  }

 
}
