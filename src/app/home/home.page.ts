import { Component } from '@angular/core';
import { IonSlides, PopoverController } from '@ionic/angular';
import { ServiceService } from '../api/service.service';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { PopoverPage } from '../page/popover/popover.page';




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  slideOptions = {
    initialSlide: 1,
    speed: 1000,
  };

  item_all: any
  last_update: any
  terkonfirm: any
  perawatan: any
  sembuh: any
  meninggal: any

  global_confirm : any
  global_active : any
  global_recovered : any
  global_death : any

  lazy : boolean = true
  date : any
  time : any
  constructor(
    public apiService: ServiceService,
    public http: HttpClient,
    private datePipe: DatePipe,
    private popOver : PopoverController
  ) {
    this.load_all()
    // this.load_by_country()
    this.load_indo()
    this.load_global()
  }
  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }

  load_all() {
    new Promise(resolve => {
      this.http.get(this.apiService.getAll())
        .toPromise()
        .then(response => {
          resolve(response)
          let res : any
          res = response
          let sort_name = res.sort((a, b) => {
            return a.countryRegion - b.countryRegion
          })
          this.item_all = sort_name
          console.log('RESPONSE API', sort_name)
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  load_global() {
    new Promise(resolve => {
      this.http.get(this.apiService.get_global())
        .toPromise()
        .then(response => {
          resolve(response)
          let res : any = response
          this.global_confirm = res.confirmed.value
          this.global_recovered = res.recovered.value
          this.global_death = res.deaths.value
          console.log('RESPONSE Global', response)
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  load_by_country() {
    new Promise(resolve => {
      this.http.get(this.apiService.getByCountry() + 'ID/confirmed')
        .toPromise()
        .then(response => {
          resolve(response)
          this.http.get(this.apiService.getByCountry() + 'ID')
          .toPromise()
          .then(res => {
            if (res){
              setTimeout(() => {
                this.lazy = false  
              }, 1000);
              
            }
            let res1 : any = res
            console.log('res last update',res1)
            this.date = res1.lastUpdate.slice(0,10)
            this.time = res1.lastUpdate.slice(11,-5)
            console.log('DATE',this.date, this.time)
            this.date = this.datePipe.transform(this.date, 'dd-MMM-yyyy');
          })
          let res2 : any = response
          console.log('RESPONSE Indo', res2)
          this.terkonfirm = res2[0].confirmed
          this.perawatan = res2[0].active
          this.sembuh = res2[0].recovered
          this.meninggal = res2[0].deaths
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  load_indo(){
    new Promise(resolve => {
      this.http.get(this.apiService.get_indo())
        .toPromise()
        .then(response => {
          if(response)
          {
            this.lazy = false
          }
          resolve(response)

          let res2 : any = response
            this.date = res2.metadata.lastUpdatedAt.slice(0,10)
            this.time = res2.metadata.lastUpdatedAt.slice(11,-6)
            console.log('DATE',this.date, this.time)
            this.date = this.datePipe.transform(this.date, 'dd-MMM-yyyy');

          console.log('RESPONSE Indo', res2)
          this.terkonfirm = res2.confirmed.value
          this.perawatan = res2.activeCare.value
          this.sembuh = res2.recovered.value
          this.meninggal = res2.deaths.value
        })
        .catch((error: any) => {
          console.log(error)
        })
    })
  }

  refresh(){
    this.lazy = true
    // this.load_by_country()
    this.load_indo()
    this.load_all()
  }

  async presentPopover(ev: any) {
    const popover = await this.popOver.create({
      component: PopoverPage,
      event: ev,
      translucent: true,
      cssClass: 'popover_class',
    });

    await popover.present();
  }
}
